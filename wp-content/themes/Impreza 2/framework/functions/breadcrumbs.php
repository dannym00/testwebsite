<?php

/**
 * Based on breadcrumbs function by Dimox
 * http://dimox.net/wordpress-breadcrumbs-without-a-plugin/
 */
if ( ! function_exists( 'us_breadcrumbs' ) ) {
	function us_breadcrumbs() {

		/* === OPTIONS === */
		$text['home'] = us_translate( 'Home' ); // text for the 'Home' link
		$text['search'] = us_translate( 'Search Results' ); // text for a search results page
		$text['404'] = us_translate( 'Page not found' ); // text for the 404 page
		$text['forums'] = us_translate( 'Forums', 'bbpress' ); // text for the forums page

		$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
		$showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
		$delimiter = ' <span class="g-breadcrumbs-separator"></span> '; // delimiter between crumbs
		$before = '<span class="g-breadcrumbs-item">'; // tag before the current crumb
		$after = '</span>'; // tag after the current crumb
		/* === END OF OPTIONS === */

		// WooCommerce product breadcrumbs
		if ( function_exists( 'woocommerce_breadcrumb' ) AND ( is_shop() OR is_product_category() OR is_product_tag() OR is_product() OR is_account_page() ) ) {
			return woocommerce_breadcrumb(
				array(
					'delimiter' => $delimiter,
					'wrap_before' => '<div class="g-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">',
					'wrap_after' => '</div>',
					'before' => $before,
					'after' => $after,
				)
			);
		}

		// bbPress breadcrumbs
		if ( function_exists( 'bbp_get_breadcrumb' ) AND in_array( get_post_type(), array( 'topic', 'forum' ) ) ) {
			return bbp_get_breadcrumb(
				array(
					'before' => '<div class="g-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">',
					'after' => '</div>',
					'sep' => $delimiter,
					'crumb_before' => $before,
					'crumb_after' => $after,
				)
			);
		}

		global $post;
		$homeLink = home_url() . '/';
		$linkBefore = '<span typeof="v:Breadcrumb">';
		$linkAfter = '</span>';
		$linkAttr = ' rel="v:url" property="v:title"';
		$link = $linkBefore . '<a class="g-breadcrumbs-item"' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
		$output = '';

		if ( is_home() || is_front_page() ) {
			if ( $showOnHome == 1 ) {
				$output .= '<div id="crumbs"><a href="' . esc_url( $homeLink ) . '">' . $text['home'] . '</a></div>';
			}
		} else {
			$output .= '<div class="g-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf( $link, $homeLink, $text['home'] ) . $delimiter;
			if ( is_category() ) {
				$thisCat = get_category( get_query_var( 'cat' ), FALSE );
				if ( $thisCat->parent != 0 ) {
					$cats = get_category_parents( $thisCat->parent, TRUE, $delimiter );
					$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, $cats );
					$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats );
					$output .= $cats;
				}
				$output .= $before . single_cat_title( '', FALSE ) . $after;
			} elseif ( is_search() ) {
				$output .= $before . $text['search'] . $after;
			} elseif ( is_day() ) {
				$output .= sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				$output .= sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), __( get_the_time( 'F' ), 'us' ) ) . $delimiter;
				$output .= $before . get_the_time( 'd' ) . $after;
			} elseif ( is_month() ) {
				$output .= sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				$output .= $before . __( get_the_time( 'F' ), 'us' ) . $after;
			} elseif ( is_year() ) {
				$output .= $before . get_the_time( 'Y' ) . $after;
			} elseif ( is_single() && ! is_attachment() ) {
				if ( get_post_type() == 'topic' OR get_post_type() == 'forum' ) {
					$forums_page = bbp_get_page_by_path( bbp_get_root_slug() );
					if ( ! empty( $forums_page ) ) {
						$forums_page_url = get_permalink( $forums_page->ID );
						$output .= sprintf( $link, $forums_page_url, $text['forums'] );
					}
					$parent_id = $post->post_parent;
					if ( $parent_id ) {
						$breadcrumbs = array();
						while ( $parent_id ) {
							$page = get_page( $parent_id );
							$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
							$parent_id = $page->post_parent;
						}
						$breadcrumbs = array_reverse( $breadcrumbs );
						for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
							$output .= $delimiter . $breadcrumbs[ $i ];
						}
					}
				} elseif ( get_post_type() == 'us_portfolio' AND us_get_option( 'portfolio_breadcrumbs_page' ) != '' ) {
					$portfolio_breadcrumbs_page = get_page_by_path( us_get_option( 'portfolio_breadcrumbs_page' ) );
					if ( $portfolio_breadcrumbs_page ) {
						if ( class_exists( 'SitePress' ) && defined( 'ICL_LANGUAGE_CODE' ) ) {
							$current_page_ID = apply_filters( 'wpml_object_id', $portfolio_breadcrumbs_page->ID, get_post_type( $portfolio_breadcrumbs_page->ID ), TRUE );
							$portfolio_breadcrumbs_page_title = get_the_title( $current_page_ID );
						} else {
							$portfolio_breadcrumbs_page_title = get_the_title( $portfolio_breadcrumbs_page->ID );
						}
						$output .= sprintf( $link, get_permalink( $portfolio_breadcrumbs_page->ID ), $portfolio_breadcrumbs_page_title );
					}
					if ( $showCurrent == 1 ) {
						$output .= $delimiter . $before . get_the_title() . $after;
					}
				} elseif ( get_post_type() != 'post' ) {
					$post_type = get_post_type_object( get_post_type() );
					if ( ! empty( $post_type->labels->name ) ) {
						$output .= $post_type->labels->name;
					}
					if ( $showCurrent == 1 ) {
						$output .= $delimiter . $before . get_the_title() . $after;
					}
				} else {
					$cat = get_the_category();
					$cat = $cat[0];
					$cats = get_category_parents( $cat, TRUE, $delimiter );
					if ( $showCurrent == 0 ) {
						$cats = preg_replace( "#^(.+)$delimiter$#", "$1", $cats );
					}
					$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, $cats );
					$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats );
					$output .= $cats;
					if ( $showCurrent == 1 ) {
						$output .= $before . get_the_title() . $after;
					}
				}
			} elseif ( function_exists( 'is_shop' ) and is_shop() ) {
				if ( ! $post->post_parent ) {
					if ( $showCurrent == 1 ) {
						$output .= $before . get_the_title() . $after;
					}
				} elseif ( $post->post_parent ) {
					$parent_id = $post->post_parent;
					$breadcrumbs = array();
					while ( $parent_id ) {
						$page = get_page( $parent_id );
						$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
						$parent_id = $page->post_parent;
					}
					$breadcrumbs = array_reverse( $breadcrumbs );
					for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
						$output .= $breadcrumbs[ $i ];
						if ( $i != count( $breadcrumbs ) - 1 ) {
							$output .= $delimiter;
						}
					}
					if ( $showCurrent == 1 ) {
						$output .= $delimiter . $before . get_the_title() . $after;
					}
				}
			} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {
				$post_type = get_post_type_object( get_post_type() );
				if ( isset( $post_type->labels->name ) ) {
					$output .= $before . $post_type->labels->name . $after;
				} else {
					if ( function_exists( 'bbp_is_single_user' ) and bbp_is_single_user() ) {
						$output .= us_translate( 'Profile', 'bbpress' );
					} else {
						$output .= '<script>jQuery(document).ready(function() { jQuery(".g-breadcrumbs-separator").last().hide(); });</script>';
					}
				}
			} elseif ( is_attachment() ) {
				$parent = get_post( $post->post_parent );
				$cat = get_the_category( $parent->ID );
				if ( is_array( $cat ) AND count( $cat ) > 0 ) {
					$cat = $cat[0];
					$cats = get_category_parents( $cat, TRUE, $delimiter );
					$cats = str_replace( '<a', $linkBefore . '<a' . $linkAttr, NULL );
					$cats = str_replace( '</a>', '</a>' . $linkAfter, $cats );
					$output .= $cats;
				}
				$output .= sprintf( $link, get_permalink( $parent ), $parent->post_title );
				if ( $showCurrent == 1 ) {
					$output .= $delimiter . $before . get_the_title() . $after;
				}
			} elseif ( is_page() && ! $post->post_parent ) {
				if ( $showCurrent == 1 ) {
					$output .= $before . get_the_title() . $after;
				}
			} elseif ( is_page() && $post->post_parent ) {
				$parent_id = $post->post_parent;
				$breadcrumbs = array();
				$front_page_id = get_option( 'page_on_front' );
				$isFrontParent = FALSE;
				while ( $parent_id ) {
					$page = get_page( $parent_id );

					if ( $front_page_id == $parent_id ) {
						// Remove double home page when home is a parent
						$parent_id = $page->post_parent;
						if ( ! $parent_id ) {
							$isFrontParent = TRUE;
						}
						continue;
					}

					$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
					$output .= $breadcrumbs[ $i ];
					if ( $i != count( $breadcrumbs ) - 1 ) {
						$output .= $delimiter;
					}
				}
				if ( $showCurrent == 1 ) {
					if ( $isFrontParent && ! count( $breadcrumbs ) ) {
						$delimiter = '';
					}
					$output .= $delimiter . $before . get_the_title() . $after;
				}
			} elseif ( is_tag() ) {
				$output .= $before . single_tag_title( '', FALSE ) . $after;
			} elseif ( is_author() ) {
				global $author;
				$userdata = get_userdata( $author );
				$output .= $before . $userdata->display_name . $after;
			} elseif ( is_404() ) {
				$output .= $before . $text['404'] . $after;
			}

			if ( get_query_var( 'paged' ) AND ! ( get_post_type() == 'topic' OR get_post_type() == 'forum' ) ) {
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
					$output .= ' (';
				} else {
					$output .= $delimiter;
				}
				$output .= us_translate( 'Page' ) . ' ' . get_query_var( 'paged' );
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
					$output .= ')';
				}
			}

			$output .= '</div>';
		}

		return $output;
	}
}
